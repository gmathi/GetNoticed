package io.github.gmathi.getnoticed.util

import android.view.View
import android.view.animation.AccelerateDecelerateInterpolator
import androidx.core.view.ViewCompat

object AnimUtils {

    fun fadeIn(view: View, durationInMillis: Long = 1000) {
        if (view.alpha == 0f)
            ViewCompat.animate(view)
                    .alpha(1f)
                    .setDuration(durationInMillis)
                    .setInterpolator(AccelerateDecelerateInterpolator())
                    .start()
    }

    fun fadeOut(view: View, durationInMillis: Long = 1000) {
        if (view.alpha == 1f)
            ViewCompat.animate(view)
                    .alpha(0f)
                    .setDuration(durationInMillis)
                    .setInterpolator(AccelerateDecelerateInterpolator())
                    .start()
    }
}