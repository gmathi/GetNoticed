package io.github.gmathi.getnoticed.util

import android.content.Context
import android.content.SharedPreferences
import android.preference.PreferenceManager


class DataCenter(context: Context) {

    private val prefs: SharedPreferences = PreferenceManager.getDefaultSharedPreferences(context)

    var areColorsChanging: Boolean
        get() = prefs.getBoolean("areColorsChanging", true)
        set(value) = prefs.edit().putBoolean("areColorsChanging", value).apply()

    var displayText: String
        get() = prefs.getString("displayText", "TAXI") ?: "TAXI"
        set(value) = prefs.edit().putString("displayText", value).apply()

//    var textSize: Int
//        get() = prefs.getInt(TEXT_SIZE, 0)
//        set(value) = prefs.edit().putInt(TEXT_SIZE, value).apply()
//
//
//
//
//
//    fun getVerifiedHosts(): ArrayList<String> =
//            Gson().fromJson(prefs.getString(VERIFIED_HOSTS, Gson().toJson(HostNames.defaultHostNamesList)), object : TypeToken<ArrayList<String>>() {}.type)


}
