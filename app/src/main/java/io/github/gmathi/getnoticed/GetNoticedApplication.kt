package io.github.gmathi.getnoticed

import android.app.Application
import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.Context
import android.os.Build
import io.github.gmathi.getnoticed.util.DataCenter

class GetNoticedApplication : Application() {

    companion object {
        var dataCenter: DataCenter? = null
        private const val TAG = "GetNoticedApplication"
    }


    override fun onCreate() {
        dataCenter = DataCenter(applicationContext)
        super.onCreate()

        //For push notifications
        initChannels(applicationContext)
    }


    private fun initChannels(context: Context) {
        if (Build.VERSION.SDK_INT < 26) {
            return
        }
        val notificationManager = context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        val channel = NotificationChannel("default",
                "Default Channel",
                NotificationManager.IMPORTANCE_DEFAULT)
        channel.description = "Default Channel Description"
        channel.lockscreenVisibility = Notification.VISIBILITY_PUBLIC
        notificationManager.createNotificationChannel(channel)
    }
}