package io.github.gmathi.getnoticed.util

import android.graphics.Color
import java.lang.reflect.Array.setFloat
import java.lang.reflect.AccessibleObject.setAccessible
import android.os.Build
import android.widget.TextView



object Utils {

    fun getComplementaryColor(colorToInvert: Int): Int {
        val hsv = FloatArray(3)
        Color.RGBToHSV(Color.red(colorToInvert), Color.green(colorToInvert),
                Color.blue(colorToInvert), hsv)
        hsv[0] = (hsv[0] + 180) % 360
        return Color.HSVToColor(hsv)
    }

    fun setMarqueeSpeed(tv: TextView, speed: Float, speedIsMultiplier: Boolean) {
        try {
            val f = tv.javaClass.getDeclaredField("mMarquee")
            f.isAccessible = true

            val marquee = f.get(tv)
            if (marquee != null) {

                val mf = marquee.javaClass.getDeclaredField("mPixelsPerSecond")
                mf.isAccessible = true

                var newSpeed = speed
                if (speedIsMultiplier)
                    newSpeed = mf.getFloat(marquee) * speed

                mf.setFloat(marquee, newSpeed)
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }

    }

}
