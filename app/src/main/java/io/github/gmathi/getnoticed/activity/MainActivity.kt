package io.github.gmathi.getnoticed.activity

import android.graphics.Color
import android.graphics.drawable.GradientDrawable
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.util.TypedValue
import android.view.View
import android.view.ViewTreeObserver
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import com.afollestad.materialdialogs.MaterialDialog
import com.afollestad.materialdialogs.callbacks.onDismiss
import com.afollestad.materialdialogs.input.input
import com.afollestad.materialdialogs.lifecycle.lifecycleOwner
import io.github.gmathi.getnoticed.R
import io.github.gmathi.getnoticed.util.Utils
import io.github.gmathi.getnoticed.util.dataCenter
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity() {

    companion object {
        private const val TAG = "MainActivity"
        private const val COLOR_CHANGE_DELAY = 300
        private var sColorCounter = 0
    }

    private lateinit var mColorValuesArray: IntArray

    private var isColorChangeRunning = dataCenter.areColorsChanging


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        viewGlobalListener()

        mColorValuesArray = resources.getIntArray(R.array.rainbow_colors)
        textView.isSelected = true
        textView.text = dataCenter.displayText
        textView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 150f)
        changeColor()
        setClickListeners()
    }

    private fun setClickListeners() {
        playIcon.setImageResource(if (isColorChangeRunning) R.drawable.ic_pause_black_24dp else R.drawable.ic_play_arrow_black_24dp)
        playIcon.setOnClickListener {
            isColorChangeRunning = !isColorChangeRunning
            dataCenter.areColorsChanging = isColorChangeRunning
            playIcon.setImageResource(if (isColorChangeRunning) R.drawable.ic_pause_black_24dp else R.drawable.ic_play_arrow_black_24dp)
            changeColor()
        }

        editTextIcon.setOnClickListener {
            val preColorCondition = isColorChangeRunning
            isColorChangeRunning = false
            MaterialDialog(this@MainActivity).show {
                title(text = "Change Display Text")
                positiveButton(text = "Submit")
                lifecycleOwner(this@MainActivity)
                input(prefill = dataCenter.displayText, allowEmpty = true, waitForPositiveButton = true) { _, text ->
                    dataCenter.displayText = text.toString()
                    this@MainActivity.textView.text = text
                }
                onDismiss {
                    isColorChangeRunning = preColorCondition
                    changeColor()
                }
            }
        }

        changeTextSizeIcon.setOnClickListener {
            seekBarParentLayout.visibility = View.VISIBLE
            seekBarDoneButton.setOnClickListener {
                seekBarParentLayout.visibility = View.GONE
            }
            seekBarParentLayout.setOnClickListener {/*Do Nothing */}



        }

    }

    private fun viewGlobalListener() {
        miniToolsLayout.viewTreeObserver.addOnGlobalLayoutListener(object : ViewTreeObserver.OnGlobalLayoutListener {
            override fun onGlobalLayout() {
                miniToolsLayout.viewTreeObserver.removeOnGlobalLayoutListener(this)
                val shape = GradientDrawable()
                shape.cornerRadius = 50f
                shape.setStroke(3, ContextCompat.getColor(this@MainActivity, R.color.DarkGray))
                shape.setColor(Color.WHITE)
                miniToolsLayout.background = shape
            }
        })
    }

    //region Color Changer Code

    private val mColorChangeHandler = Handler()
    private val mChangeColorRunnable = Runnable {
        mainLayout.setBackgroundColor(mColorValuesArray[sColorCounter])
        textView.setTextColor(Utils.getComplementaryColor(mColorValuesArray[sColorCounter]))
        ++sColorCounter
        if (sColorCounter >= mColorValuesArray.size)
            sColorCounter = 0
        changeColor()
    }

    private fun changeColor() {
        mColorChangeHandler.removeCallbacks(mChangeColorRunnable)
        if (isColorChangeRunning)
            mColorChangeHandler.postDelayed(mChangeColorRunnable, COLOR_CHANGE_DELAY.toLong())
    }

    //endregion


    //region FullScreen Code
    override fun onWindowFocusChanged(hasFocus: Boolean) {
        super.onWindowFocusChanged(hasFocus)
        if (hasFocus) hideSystemUI()
    }

    private fun hideSystemUI() {
        var immersiveModeOptions = (View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                or View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                or View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                or View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                or View.SYSTEM_UI_FLAG_FULLSCREEN)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            immersiveModeOptions = (immersiveModeOptions or View.SYSTEM_UI_FLAG_IMMERSIVE)
        }

        window.decorView.systemUiVisibility = immersiveModeOptions
    }

    //endregion

    //region SpotLight Code


    //endregion
}
