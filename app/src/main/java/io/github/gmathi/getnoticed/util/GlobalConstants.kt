package io.github.gmathi.getnoticed.util

import io.github.gmathi.getnoticed.GetNoticedApplication

val dataCenter: DataCenter by lazy {
    GetNoticedApplication.dataCenter!!
}
