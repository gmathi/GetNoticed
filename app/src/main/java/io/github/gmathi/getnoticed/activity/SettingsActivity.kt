package io.github.gmathi.getnoticed.activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import io.github.gmathi.getnoticed.R

class SettingsActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_settings)
    }
}
